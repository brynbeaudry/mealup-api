const { makeExtendSchemaPlugin, gql } = require("graphile-utils");
const jwt = require("jsonwebtoken");

const PassportLoginPlugin = makeExtendSchemaPlugin(build => ({
  typeDefs: gql`
    input RegisterInput {
      email: String!
      password: String!
    }
    type RegisterPayload {
      user: User! @recurseDataGenerators
    }
    input LoginInput {
      email: String!
      password: String!
    }
    type LoginPayload {
      user: User! @recurseDataGenerators
    }
    extend type Mutation {
      register(input: RegisterInput!): RegisterPayload
      login(input: LoginInput!): LoginPayload
    }
  `,
  resolvers: {
    Mutation: {
      async register(
        mutation,
        args,
        context,
        resolveInfo,
        { selectGraphQLResultFromTable }
      ) {
        const { password, email, } = args.input;
        const { rootPgPool, login, pgClient } = context;
        try {
          // Call our login function to find out if the username/password combination exists
          const {
            rows: [user],
          } = await rootPgPool.query(
            `select users.* from app_private.really_create_user(
              email => $1,
              email_is_verified => false,
              first_name => null,
              last_name => null,
              language => null,
              password => $2
            ) users`,
            [email, password]
          );
          //console.log(`Result : ${JSON.stringify(user)}`)

          if (!user) {
            throw new Error("Registration failed");
          }

          // Fetch the data that was requested from GraphQL, and return it
          const sql = build.pgSql;
          const [row] = await selectGraphQLResultFromTable(
            sql.fragment`app_public.users`,
            (tableAlias, sqlBuilder) => {
              sqlBuilder.where(
                sql.fragment`${tableAlias}.id = ${sql.value(user.id)}`
              );
            }
          );
          console.log(`Row: ${JSON.stringify(row)}`)
          return {
              user : row
          }
          // Return token
          /*
          const { rows } = await rootPgPool.query(
          `select * from app_private.jwt_from_user_id($1) users`,
          [
              user.id,
          ]
          );
          let jwt_token = rows[0] || false;
          jwt_token = jwt.sign(jwt_token, process.env.JWT_SECRET)
          
          console.log(`JWT : ${JSON.stringify(jwt_token)}`)
          return {
              "token": jwt_token
          }
          */
        } catch (e) {
          console.error(e);
          // TODO: check that this is indeed why it failed
          throw new Error("Login failed: incorrect username/password");
        }
      },
      async login(
        mutation,
        args,
        context,
        resolveInfo,
        { selectGraphQLResultFromTable }
      ) {
        const { email, password } = args.input;
        const { rootPgPool, login, pgClient } = context;
        try {
          // Call our login function to find out if the username/password combination exists
          const {
            rows: [user],
          } = await rootPgPool.query(
            `select users.* from app_private.login($1, $2) users`,
            [email, password]
          );

          if (!user) {
            throw new Error("Login failed");
          }

          /* // Tell Passport.js we're logged in
          await login(user);
          // Tell pg we're logged in
          await pgClient.query("select set_config($1, $2, true);", [
            "jwt.claims.user_id",
            user.id,
          ]); */

          // Fetch the data that was requested from GraphQL, and return it
          const sql = build.pgSql;
          const [row] = await selectGraphQLResultFromTable(
            sql.fragment`app_public.users`,
            (tableAlias, sqlBuilder) => {
              sqlBuilder.where(
                sql.fragment`${tableAlias}.id = ${sql.value(user.id)}`
              );
            }
          );
          return {
            user: row,
          };
        } catch (e) {
          console.error(e);
          // TODO: check that this is indeed why it failed
          throw new Error("Login failed: incorrect username/password");
        }
      },
    },
  },
}));
module.exports = PassportLoginPlugin;