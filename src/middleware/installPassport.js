const passport = require("koa-passport");
const route = require("koa-route");
const { Strategy: FacebookStrategy } = require("passport-facebook");
const { Strategy: GoogleStrategy } = require("passport-google-oauth20");
const { Strategy: LocalStrategy } = require("passport-local");
const jwt = require("jsonwebtoken");

/*
 * This file uses regular Passport.js authentication, both for
 * username/password and for login with GitHub. You can easily add more OAuth
 * providers to this file. For more information, see:
 *
 *   http://www.passportjs.org/
 */

module.exports = function installPassport(app, router, { rootPgPool }) {
    passport.serializeUser((user, done) => {
        done(null, user.id);
    });

    passport.deserializeUser(async (id, callback) => {
        let error = null;
        let user;
        try {
        const {
            rows: [_user],
        } = await rootPgPool.query(
            `select users.* from app_public.users where users.id = $1`,
            [id]
        );
        user = _user || false;
        } catch (e) {
            error = e;
        } finally {
            callback(error, user);
        }
    });
    app.use(passport.initialize());
    //app.use(passport.session());


    const handlePassportUser = async (err, user, info) => {
        // get jwt from user
        if (err || !user) {
            ctx.status = 400;
            return ctx.body = {
                message: 'Login failed',
                error: err
            }
        }
        // you have the user, send the token
        //use the get jwt from user function 
        try {
            const { rows } = await rootPgPool.query(
            `select * from app_private.jwt_from_user_id($1) users`,
            [
                user.id,
            ]
            );
            let jwt_token = rows[0] || false;
            jwt_token = jwt.sign(jwt_token, process.env.JWT_SECRET)
            
            //console.log(`JWT : ${JSON.stringify(jwt_token)}`)
            ctx.status = 200
            return ctx.body = {
                "token": jwt_token
            }
        } catch (e) {
            console.log(`Error: ${e}`)
            ctx.status = 400;
            return ctx.body = {
                message: 'Something is not right',
                error   : e
            }
        }
        // could enevitably redirect to the client here.
        //res.cookie('auth', token); // Choose whatever name you'd like for that cookie, 
        //res.redirect('http://localhost:3000');
    }

    passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    }, 
    async (email, password, callback) => {
        let error = null;
        let user;
        //console.log(`In local strategy email : ${email}, password : ${password}`)
        //this one is typically a DB call. Assume that the returned user object is pre-formatted and ready for storing in JWT
        try {
            const {
              rows: [_user],
            } = await rootPgPool.query(
                `select users.* from app_private.login($1, $2) users`,
                [email, password]
            );
            console.log(`User: ${JSON.stringify(_user)}`)
            user = _user || false;
            if (!user) {
                throw new Error("Login Failed")
            }
          } catch (e) {
            console.log(e.message)
            error = e.message;
          } finally {
            callback(error, user);
          }
        }
    ));
    
    router.post('/auth/local', async (ctx, next) => {
        // console.log(`In auth local`)
        await passport.authenticate('local', {session: false}, async (err, user, info) => {
            //console.log(`In pass auth local`)
            //console.log(`User: ${JSON.stringify(user)} Error: ${err}`)
            if (err || !user) {
                ctx.status = 400;
                return ctx.body = {
                    message: 'Login failed',
                    error: err
                }
            }
            // you have the user, send the token
            //use the get jwt from user function 
            try {
                const { rows } = await rootPgPool.query(
                `select * from app_private.jwt_from_user_id($1) users`,
                [
                    user.id,
                ]
                );
                let jwt_token = rows[0] || false;
                jwt_token = jwt.sign(jwt_token, process.env.JWT_SECRET)
                
                //console.log(`JWT : ${JSON.stringify(jwt_token)}`)
                ctx.status = 200
                return ctx.body = {
                    "token": jwt_token
                }
            } catch (e) {
                console.log(`Error: ${e}`)
                ctx.status = 400;
                return ctx.body = {
                    message: 'Something is not right',
                    error   : e
                }
            }
        })(ctx, next)
    });

    // FACEBOOK
    if (process.env.FACEBOOK_KEY != "" && process.env.FACEBOOK_SECRET) {
        passport.use(
            new FacebookStrategy(
            {
                clientID: process.env.FACEBOOK_KEY,
                clientSecret: process.env.FACEBOOK_SECRET,
                callbackURL: `${process.env.ROOT_URL}/auth/facebook/callback`,
                profileFields: ['id', 'displayName', 'link', 'emails'],
                passReqToCallback: true,
            },
            async function(req, accessToken, refreshToken, profile, done) {
                let error;
                let user;
                console.log(`Profile : ${JSON.stringify(profile)}`)
                const nameArray = profile.displayName.split(" ")
                console.log(`Name Array 0 or null : ${JSON.stringify(nameArray[0] || null)}`)

                try {
                const { rows } = await rootPgPool.query(
                    `select * from app_private.link_or_register_user($1, $2, $3, $4, $5) users where not (users is null);`,
                    [
                    (req.user && req.user.id) || null,
                    "facebook",
                    profile.id,
                    JSON.stringify({
                        email: profile.emails[0] && profile.emails[0].value || profile.displayName.toLowerCase() + '@facebook.com',
                        first_name: nameArray[0] || null,
                        last_name: (nameArray.length > 1)? nameArray[nameArray.length - 1] : null,
                        language: profile.language || 'en'
                    }),
                    JSON.stringify({
                        accessToken,
                        refreshToken,
                    }),
                    ]
                );
                user = rows[0] || false;
                } catch (e) {
                    error = e;
                } finally {
                    done(error, user);
                }
            }
            )
        );

        app.use(route.get("/auth/facebook", passport.authenticate("facebook", {scope: ['email']})));

        router.get("/auth/facebook/callback",
            async (ctx, next) => {
            await passport.authenticate("facebook", {
                //successRedirect: "/",
                //failureRedirect: "/",
                session: false
            }, async (err, user, info) => {
                // get jwt from user
                if (err || !user) {
                    ctx.status = 400;
                    return ctx.body = {
                        message: 'Login failed',
                        error: err
                    }
                }
                // you have the user, send the token
                //use the get jwt from user function 
                try {
                    const { rows } = await rootPgPool.query(
                    `select * from app_private.jwt_from_user_id($1) users`,
                    [
                        user.id,
                    ]
                    );
                    let jwt_token = rows[0] || false;
                    jwt_token = jwt.sign(jwt_token, process.env.JWT_SECRET)
                    
                    //console.log(`JWT : ${JSON.stringify(jwt_token)}`)
                    ctx.status = 200
                    return ctx.body = {
                        "token": jwt_token
                    }
                } catch (e) {
                    console.log(`Error: ${e}`)
                    ctx.status = 400;
                    return ctx.body = {
                        message: 'Something is not right',
                        error   : e
                    }
                }
                // could enevitably redirect to the client here.
                //res.cookie('auth', token); // Choose whatever name you'd like for that cookie, 
                //res.redirect('http://localhost:3000');
            })(ctx, next)
        })
    } else {
        console.error(
            "WARNING: you've not set up the Facebook application for login; see `.env` for details"
        );
    }

    // GOOGLE
    if (process.env.GOOGLE_KEY != "" && process.env.GOOGLE_SECRET) {
        passport.use(
            new GoogleStrategy(
            {
                clientID: process.env.GOOGLE_KEY,
                clientSecret: process.env.GOOGLE_SECRET,
                callbackURL: `${process.env.ROOT_URL}/auth/google/callback`,
                passReqToCallback: true,
            },
            async function(req, accessToken, refreshToken, profile, done) {
                console.log(`Profile : ${JSON.stringify(profile)}`)
                let error;
                let user;
                console.log(`Profile : ${JSON.stringify(profile)}`)
                const nameArray = profile.displayName.split(" ")
                console.log(`Name Array 0 or null : ${JSON.stringify(nameArray[0] || null)}`)
                try {
                const { rows } = await rootPgPool.query(
                    `select * from app_private.link_or_register_user($1, $2, $3, $4, $5) users where not (users is null);`,
                    [
                    (req.user && req.user.id) || null,
                    "google",
                    profile.id,
                    JSON.stringify({
                        email: profile.emails[0] && profile.emails[0].value || profile.displayName.toLowerCase() + '@google.com',
                        first_name: nameArray[0] || null,
                        last_name: (nameArray.length > 1)? nameArray[nameArray.length - 1] : null,
                        language: profile.language || 'en'
                    }),
                    JSON.stringify({
                        accessToken,
                        refreshToken,
                    }),
                    ]
                );
                user = rows[0] || false;
                } catch (e) {
                    error = e;
                    console.log(`DB error : ${error}`)
                } finally {
                    done(error, user);
                }
            })
        );

        app.use(route.get("/auth/google", passport.authenticate("google", {scope: ['email']})));

        router.get("/auth/google/callback",
            async (ctx, next) => {
                await passport.authenticate("google", {
                    //successRedirect: "/",
                    //failureRedirect: "/",
                    session: false
                }, async (err, user, info) => {
                    // get jwt from user
                    if (err || !user) {
                        ctx.status = 400;
                        return ctx.body = {
                            message: 'Login failed',
                            error: err
                        }
                    }
                    // you have the user, send the token
                    //use the get jwt from user function 
                    try {
                        const { rows } = await rootPgPool.query(
                        `select * from app_private.jwt_from_user_id($1) users`,
                        [
                            user.id,
                        ]
                        );
                        let jwt_token = rows[0] || false;
                        jwt_token = jwt.sign(jwt_token, process.env.JWT_SECRET)
                        
                        //console.log(`JWT : ${JSON.stringify(jwt_token)}`)
                        ctx.status = 200
                        return ctx.body = {
                            "token": jwt_token
                        }
                    } catch (e) {
                        console.log(`Error: ${e}`)
                        ctx.status = 400;
                        return ctx.body = {
                            message: 'Something is not right',
                            error   : e
                        }
                    }
                    // could enevitably redirect to the client here.
                    //res.cookie('auth', token); // Choose whatever name you'd like for that cookie, 
                    //res.redirect('http://localhost:3000');
                })(ctx, next)
            })
    } else {
        console.error(
            "WARNING: you've not set up the Google application for login; see `.env` for details"
        );
    }
    app.use(
        route.get("/logout", async ctx => {
        ctx.logout();
        ctx.redirect("/");
        })
    );
};
