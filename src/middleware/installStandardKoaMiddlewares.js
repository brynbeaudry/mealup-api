const helmet = require("koa-helmet");
const cors = require("@koa/cors");
const jwt = require("koa-jwt");
const compress = require("koa-compress");
const bunyanLogger = require("koa-bunyan-logger");
const bodyParser = require("koa-bodyparser");
const convert = require('koa-convert')

module.exports = function installStandardKoaMiddlewares(app) {
  // These middlewares aren't required, I'm using them to check PostGraphile
  // works with Koa.
  app.use(helmet());
  // be sure to include the client and api production sites here.
  const whitelist = ["http://localhost:8080", "http://localhost:3000", "http://localhost:8349"];
  function checkOriginAgainstWhitelist(ctx) {
    const requestOrigin = ctx.accept.headers.origin;
    if (!whitelist.includes(requestOrigin)) {
      return ctx.throw(`${requestOrigin} is not a valid origin`);
    }
    return requestOrigin;
  }
  app.use(convert(cors({ origin: checkOriginAgainstWhitelist })));
  //app.use(jwt({secret: process.env.SECRET}))
  app.use(compress());
  app.use(bunyanLogger());
  app.use(bodyParser());
};
