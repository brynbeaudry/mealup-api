const { postgraphile } = require("postgraphile");
const PassportLoginPlugin = require("../plugins/PassportLoginPlugin");
const {
  library: { connection, schema, options },
} = require("../../.postgraphilerc.js");``
const jwt = require("jsonwebtoken");

module.exports = function installPostGraphile(app, { rootPgPool }) {
  app.use((ctx, next) => {
    // PostGraphile deals with (req, res) but we want access to sessions from `pgSettings`, so we make the ctx available on req.
    ctx.req.ctx = ctx;
    return next();
  });

  app.use(
    postgraphile(connection, schema, {
      // Import our shared options
      ...options,

      // Since we're using sessions we'll also want our login plugin
      appendPlugins: [
        // All the plugins in our shared config
        ...(options.appendPlugins || []),

        // Adds the `login` mutation to enable users to log in
        PassportLoginPlugin,
      ],
      jwtSecret: process.env.JWT_SECRET,
      jwtPgTypeIdentifier: 'app_public.jwt_token',
      jwtVerifyOptions: {
        audience : ['mealup']
      },

      // Given a request object, returns the settings to set within the
      // Postgres transaction used by GraphQL.
      pgSettings(req) {
        //console.log(`-------------------------------------------------------------------`)
        //console.log(JSON.stringify(req.headers))
        let authHeader = req.headers["authorization"]
        let token = (authHeader && authHeader.startsWith("Bearer ")) ? authHeader.substring(7, authHeader.length) : null
        //console.log(`Token: ${token}`)
        let jwt_user = (token) ? jwt.verify(token, process.env.JWT_SECRET) : null;
        //console.log(jwt_user)
        return {

          "jwt.claims.user_id": jwt_user && jwt_user.user_id,
          // Todo, include whether a user is an admin in their token
          'role': (jwt_user && jwt_user.user_id) ? 'mealup_user' : 'mealup_visitor',
          //'role': 'mealup_superuser',
          //'http.headers': `${req.headers['x-something'] || ''}`,
          'http.method': `${req.method}`,
          'http.url': `${req.url}`,
        };
      },

      // The return value of this is added to `context` - the third argument of
      // GraphQL resolvers. This is useful for our custom plugins.
      additionalGraphQLContextFromRequest(req) {
        return {
          // Let plugins call priviliged methods (e.g. login) if they need to
          rootPgPool,

          // Use this to tell Passport.js we're logged in
          // Don't use this
          login: user =>
            new Promise((resolve, reject) => {
              req.ctx.login(user, err => (err ? reject(err) : resolve()));
            }),
        };
      },
    })
  );
};
