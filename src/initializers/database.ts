const path = require('path');
import { createConnection, DefaultNamingStrategy, NamingStrategyInterface } from 'typeorm';
import { snakeCase } from 'typeorm/util/StringUtils';
import { 
    Company,
    CompanyStaff,
    CompanyStaffRequest,
    DietaryRestriction,
    Event,
    Image,
    Item,
    ItemAttribute,
    ItemCategory,
    ItemPortion,
    Location,
    Order,
    OrderItem,
    Payment,
    SocialHandle,
    User,
    UserProfile
} from '../entities';

// Keeps entity attributes camelCase and database attributes snake case
class CustomNamingStrategy extends DefaultNamingStrategy implements NamingStrategyInterface {

    tableName(targetName: string, userSpecifiedName: string): string {
        return userSpecifiedName ? userSpecifiedName : snakeCase(targetName);
    }

    columnName(propertyName: string, customName: string, embeddedPrefixes: string[]): string {
        return snakeCase(embeddedPrefixes.concat(customName ? customName : propertyName).join("_"));
    }

    columnNameCustomized(customName: string): string {
        return customName;
    }

    relationName(propertyName: string): string {
        return snakeCase(propertyName);
    }
}


export const databaseInitializer = async () => {
    console.log(`Root pass: ${process.env.SUPERUSER_PASSWORD}`)
    console.log(`Root pass: ${process.env.CONNECTION_STRING}`)

    return await createConnection({
        name     : "mealup",
        type     : "postgres",
        host     : process.env.CONNECTION_STRING,
        port     : 5432,
        username : "mealup",
        password : process.env.ROOT_PASSWORD,
        database : "mealup",
        namingStrategy: new CustomNamingStrategy(),
        entities: [
            Company,
            CompanyStaff,
            CompanyStaffRequest,
            DietaryRestriction,
            Event,
            Image,
            Item,
            ItemAttribute,
            ItemCategory,
            ItemPortion,
            Location,
            Order,
            OrderItem,
            Payment,
            SocialHandle,
            User,
            UserProfile
        ],
        logging: ["query", "error"],
        synchronize: true,
    }).catch((error) => {
        console.log(error)
    }).then((connection) => {
       // This just means you connected to the database and everything worked
       // You could technically do some seeding
        console.log('Entities Updated');
    });

};