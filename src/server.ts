import * as Koa from 'koa';
import * as pg from 'pg'
import * as http from 'http'
import * as fs from 'fs'
import * as https from 'https'
import * as Router from 'koa-router'
import * as sharedUtils from './utils/utils.js'
import * as middleware from './middleware'


sharedUtils.sanitiseEnv();

console.log(`Connection String : ${process.env.CONNECTION_URL}`)

const rootPgPool = new pg.Pool({
  connectionString: process.env.CONNECTION_URL,
});

const isDev = process.env.NODE_ENV === "development";

// We're using a non-super-user connection string, so we need to install the
// watch fixtures ourself.
if (isDev) {
  sharedUtils.installWatchFixtures(rootPgPool);
}

const app = new Koa();
var router = new Router();


var base = process.env.PWD
//var privateKey = fs.readFileSync(base + '/certs/server.key', 'utf8')
//var certificate = fs.readFileSync(base + '/certs/server.cert', 'utf8')

//var credentials = { key: privateKey, cert: certificate }
const server = http.createServer(app.callback());
//const server = https.createServer(credentials, app.callback());

middleware.installStandardKoaMiddlewares(app);
//middleware.installSession(app);
middleware.installPassport(app, router, { rootPgPool });
middleware.installPostGraphile(app, { rootPgPool });
//middleware.installSharedStatic(app);
//middleware.installFrontendServer(app, server);

console.log(`Router routes: ${JSON.stringify(router)}`)

app
  .use(router.routes())
  .use(router.allowedMethods());

const PORT = parseInt(process.env.PORT, 10) || 3000;
server.listen(PORT);
console.log(`Listening on port ${PORT}`);
