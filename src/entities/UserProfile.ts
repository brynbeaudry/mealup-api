import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    OneToOne,
    JoinColumn,
} from 'typeorm';
import { User } from './User'
import { Image } from './Image';
import { Location } from './Location';
import { SocialHandle } from './SocialHandle';
import { DietaryRestriction } from './DietaryRestriction';

// Each user can have many user profiles, but each user rpofile only has one user.

@Entity('user_profiles', {schema: 'app_public'})
export class UserProfile {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @Column('text', {
        nullable: true,
    })
    placeOfBirth: string;

    @OneToOne(type => User, user => user.userProfile)
    @JoinColumn()
    user: User;

    @Column('text')
    bio: string;

    @OneToOne(type => Location)
    @JoinColumn()
    location: Location;
    
    // s3
    @OneToOne(type => Image)
    @JoinColumn()
    profileImage: Image;

    @OneToOne(type => SocialHandle)
    @JoinColumn()
    socialHandle: SocialHandle


    @OneToOne(type => DietaryRestriction)
    @JoinColumn()
    dietaryRestrictions: DietaryRestriction

}