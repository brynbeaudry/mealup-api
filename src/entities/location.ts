import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    OneToMany
} from 'typeorm';
import { Image } from './Image';

@Entity('locations', {schema: 'app_public'})
export class Location {

    @PrimaryGeneratedColumn('uuid')
    id: string;
    
    @Column('decimal', {
        precision: 9,
        scale: 6
    })
    latitude: number;

    @Column('decimal', {
        precision: 9,
        scale: 6
    })
    longitude: number;

    @Column('text')
    city: string;

    @Column('text')
    state: string;

    @Column('text')
    country: string;

    @Column('text')
    address: string;

    @Column('text')
    postalCode: string;

    @Column('text', {nullable:true})
    name : string;

    @Column('text', {nullable:true})
    description : string;

}