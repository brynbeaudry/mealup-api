import { Payment } from './Payment';
import { Event } from './Event';
import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    JoinColumn,
    OneToMany,
    OneToOne
} from 'typeorm';
import { OrderItem } from './OrderItem';
import { User } from './User';
import { Company } from './Company';

// Order items reference the order item
@Entity('orders', {schema: 'app_public'})
export class Order {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @ManyToOne(type => User, user => user.orders)
    @JoinColumn()
    customer: User;

    // Could get the company through the event
    @ManyToOne(type => Event, event => event.orders)
    @JoinColumn()
    event : Event

    @ManyToOne(type => Company, company => company.orders)
    @JoinColumn()
    company : Company

    @OneToMany(type => OrderItem, orderItems => orderItems.order)
    orderItems: OrderItem[];

    @Column('text', {nullable: true})
    description: string;

    @Column('decimal', {
        precision: 19,
        scale: 4
    })
    total: number;

    @Column('decimal', {
        precision: 19,
        scale: 4
    })
    subtotal: number 

    @Column('decimal', {
        precision: 19,
        scale: 4
    })
    tax: number 

    // Should be Enum, PENDING, FULLFILLED, CANCELLED 
    @Column('text')
    status: string;

    // Should be Enum, DONATION, SALE
    @Column('text')
    type: string;

    @OneToOne(type => Payment, payment => payment.order)
    payment : Payment


}