const fs = require("fs");

const entities = fs
  .readdirSync(__dirname)
  .filter(fn => fn !== "index.ts")
  .filter(fn => fn !== "entity_template.ts")
  .filter(fn => fn !== "generate.ts")
  .filter(fn => fn.match(/^[^.].*\.ts$/))
  .map(str => str.substr(0, str.length - 3));

require("fs").writeFile(
  __dirname + '/index.ts',
  entities.map(function(v){ return `export { ${v} } from "./${v}"`}).join('\n'),
  function (err) { console.log(err ? 'Error :'+err : 'ok') }
);