import { Item } from './Item';
import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany,
    ManyToMany,
    OneToOne,
    JoinColumn,
} from 'typeorm';


@Entity('item_portions', {schema: 'app_public'})
export class ItemPortion {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @OneToOne(type => Item, item => item.portion)
    @JoinColumn()
    item : Item;

    /*
    {
        unit: 'inches'
        value: null
        measurement: 'volume' //can only be one of two
        length: '6'
        width: '3'
        height: '3'
    }
    */
    @Column('jsonb')
    size: JSON;

    /*
    {
        unit: 'grams'
        value: '500'
        measurement: 'weight'  // can only be one of two    
        length: null
        width: null
        height: null
    }
    */
    @Column('jsonb')
    normalizedSize: JSON;


    @Column('decimal', {
        precision: 19,
        scale: 4
    })
    cost: number;

    @Column('bool', { default: false })
    isPackaged: boolean;

    // Should be enum
    @Column('text', { nullable: true })
    packageType: string;
}