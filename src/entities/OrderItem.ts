import {
    Entity,
    Column,
    ManyToOne,
    OneToOne,
    JoinColumn,
    PrimaryColumn
} from 'typeorm';
import { Order } from './Order';
import { Item } from './Item';
import { User } from './User';


@Entity('order_items', {schema: 'app_public'})
export class OrderItem {
    
    @PrimaryColumn('uuid') 
    orderId: string;

    @ManyToOne(type => Order, order => order.orderItems)
    @JoinColumn({ name: "order_id" })
    order: Order;

    @PrimaryColumn('uuid') 
    itemId: string;

    @ManyToOne(type => Item)
    @JoinColumn({ name: "item_id" })
    item: Item;

    @Column('decimal', {
        precision: 19,
        scale: 4
    })
    cost: number;

    @Column('smallint')
    portionQuantity: number;



}