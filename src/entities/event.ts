import { Order } from './Order';
import { Item } from './Item';
import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    OneToOne,
    JoinColumn,
    OneToMany,
} from 'typeorm';
import { User } from './User';
import { Location } from './Location';
import { Image } from './Image';
import { Company } from './Company';

@Entity('events', {schema: 'app_public'})
export class Event {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @Column('text')
    name: string;

    @Column('text')
    description: string;

    @Column('text')
    type: string;

    @Column('timestamptz')
    startTime: Date;

    @Column('timestamptz')
    endTime: Date;

    //locaiton id
    @OneToOne(type => Location)
    @JoinColumn()
    location: Location;

    @OneToMany(type => Item, item => item.event)
    items : Item[];

    //creator createdBy
    @ManyToOne(type => User, user => user.events)
    @JoinColumn({ name: "created_by" })
    createdBy: User;

    //related company
    @ManyToOne(type => Company, company => company.events)
    @JoinColumn()
    company: Company;

    @Column('text')
    pickupInstructions: string;

    @OneToOne(type=> Image)
    @JoinColumn()
    bannerImage: Image


    @OneToMany(type => Order, order => order.event)
    orders : Order[];

}