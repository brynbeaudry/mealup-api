import { ItemAttribute } from './ItemAttribute';
import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany,
    ManyToMany,
    JoinTable,
    OneToOne,
    ManyToOne,
    JoinColumn,
} from 'typeorm';

// Can only be editable by admin
// Should write these in typeorm format script at the beginning
@Entity('item_categories', {schema: 'app_public'})
export class ItemCategory {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('text')
    name: string;

    @Column('text', { nullable: true})
    description: string;

    @ManyToMany(type => ItemAttribute, attribute => attribute.category)
    @JoinTable({name : 'item_category_attributes'})
    attributes : ItemAttribute[];

    @ManyToOne(type => ItemCategory)
    @JoinColumn()
    parent : ItemCategory

}