import { Column, CreateDateColumn, Entity, OneToMany, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Event } from './Event';
import { Image } from "./Image";
import { Order } from './Order';
import { Payment } from './Payment';
import { UserProfile } from './UserProfile';

// User profile should be created with the user.
@Entity('users', {schema: 'app_public'})
export class User {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    //Each user has one user profile
    @OneToOne(type => UserProfile, userProfile => userProfile.user, { nullable: true })
    userProfile: UserProfile;

    @OneToMany(type => Order, order => order.customer, { nullable: true })
    orders: Order[];

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    // Is used as username and use unique
    @Column('text', { unique: true})
    email: string;

    @Column('text',{ nullable: true })
    firstName: string;

    @Column('text', { nullable: true })
    lastName: string;

    @Column('text', {default : "en", nullable: true})
    language: string;

    @OneToMany(type => Event, event => event.createdBy)
    events : Event[];

    //Image that the user has uploaded, regardless of which entity it was uploaded for
    @OneToMany(type => Image, image => image.uploadedBy)
    images : Image[];

    @OneToMany(type => Payment, payment => payment.user)
    payments : Payment[];

}