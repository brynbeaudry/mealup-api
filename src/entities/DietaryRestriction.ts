import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
} from 'typeorm';


@Entity('dietary_restrictions', {schema: 'app_public'})
export class DietaryRestriction {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('bool', { default: false })
    isGlutenFree: boolean;

    @Column('bool', { default: false })
    isDairyFree: boolean;

    @Column('bool', { default: false })
    isPeanutFree: boolean;

    @Column('bool', { default: false })
    isVegetarian: boolean;

    @Column('bool', { default: false })
    isVegan: boolean;

    @Column('bool', { default: false })
    isShellFishFree: boolean;
}