import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany
} from 'typeorm';

@Entity('social_handle', {schema: 'app_public'})
export class SocialHandle {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @Column('text', {
        nullable: true,
    })
    facebook: string;

    @Column('text', {
        nullable: true,
    })
    instagram: string;

    @Column('text', {
        nullable: true,
    })
    google_plus: string;

    @Column('text', {
        nullable: true,
    })
    twitter: string;

    @Column('text', {
        nullable: true,
    })
    youtube: string;

    @Column('text', {
        nullable: true,
    })
    website: string;
}
