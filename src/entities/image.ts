import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    JoinColumn,
} from 'typeorm';
import { User } from "./User";

@Entity('images', {schema: 'app_public'})
export class Image {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('text')
    name: string;

    @Column('int')
    height: number

    @Column('int')
    width: number


    @Column('text')
    description: string;

    @Column('text')
    filename: string;

    @Column('text')
    url: string;

    // One user is the uploader of many Images
    @ManyToOne(type => User, user => user.images)
    @JoinColumn({ name: "uploaded_by" })
    uploadedBy: User;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

}