import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany,
    ManyToMany,
    OneToOne,
    JoinColumn,
    ManyToOne,
} from 'typeorm';
import { Company } from './Company';
import { User } from './User';

// Company staff can only be edited by an admin of the company - needs special permissions
// You can use this to display the staff of the company, if they are approved.
// Admin is automatically made the first company staff entry self approved, or approved by Bryn
@Entity('company_staff_requests', {schema: 'app_public'})
export class CompanyStaffRequest {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @OneToOne(type=> User)
    @JoinColumn()
    user: User;

    @ManyToOne(type => Company, company => company.staff)
    company: Company

    @Column('text')
    position: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;
}