import { ItemCategory } from './ItemCategory';
import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToMany,
} from 'typeorm';

// Can only be editable by admin
@Entity('item_attributes', {schema: 'app_public'})
export class ItemAttribute {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('text')
    name: string;

    @Column('text', {nullable: true})
    description: string;

    @ManyToMany(type => ItemCategory, category => category.attributes)
    category : ItemCategory[];

    //Add categories
}