import { Order } from './Order';
import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany,
    ManyToMany,
    OneToOne,
    JoinColumn,
    ManyToOne,
} from 'typeorm';
import { Location } from './Location';
import { SocialHandle } from './SocialHandle';
import { User } from './User';
import { CompanyStaff } from './CompanyStaff';
import { Image } from './Image';
import { Event } from './Event';


@Entity('companies', {schema: 'app_public'})
export class Company {

    // Id is number because it is used in business logic to determine permisisons of user.
    @PrimaryGeneratedColumn()
    id: number;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @OneToOne(type => Location)
    @JoinColumn()
    location: Location;

    @OneToOne(type => SocialHandle)
    @JoinColumn()
    socialHandle: SocialHandle;

    @OneToMany(type => CompanyStaff, companyStaff => companyStaff.company)
    staff : CompanyStaff[];

    @Column('text')
    description: string;

    @Column('text')
    pickupInstructions: string;

    // ENUM : hospital, caterer, cafeteria, restaurant, should be a number
    @Column('text')
    type: string;

    @OneToOne(type => Image)
    @JoinColumn()
    image: Image;

    @OneToMany(type => Event, event => event.company)
    events : Event[]

    @OneToMany(type => Order, order => order.event)
    orders : Order[];
}