import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany,
    ManyToMany,
    OneToOne,
    ManyToOne,
    JoinColumn,
} from 'typeorm';
import { User } from './User';
import { Order } from './Order';

// stripe info might go in here down the line
@Entity('payments', {schema: 'app_public'})
export class Payment {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @ManyToOne(type => User, user => user.payments)
    @JoinColumn()
    user: User;

    @OneToOne(type => Order)
    @JoinColumn()
    order : Order;

    @Column('decimal', {
        precision: 19,
        scale: 4
    })
    amount: number 

    // Should be enum VISA MASTERCARD PAYPAL AMEX STRIPE
    @Column('text')
    type: string;
    
}