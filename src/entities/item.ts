import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    ManyToMany,
    OneToOne,
    JoinColumn,
    OneToMany
} from 'typeorm';
import { Image } from './Image';
import { ItemAttribute } from './ItemAttribute';
import { ItemPortion } from './ItemPortion';
import { Event } from './Event';
import { DietaryRestriction } from './DietaryRestriction';


@Entity('items', {schema: 'app_public'})
export class Item {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('text')
    name: string;

    @Column('text')
    description: string;

    @OneToOne(type => Image)
    @JoinColumn()
    image: Image;

    @OneToOne(type => ItemPortion, portion => portion.item)
    portion : ItemPortion;

    @ManyToOne(type => Event, event => event.items)
    @JoinColumn()
    event : Event;

    @Column('smallint', {default: 1})
    portionsTotal: number

    @Column('smallint', {default: 1})
    portionsAvailable: number

    @Column('smallint', {default: 1})
    portionsPerPerson: number

    @OneToOne(type => DietaryRestriction)
    @JoinColumn()
    dietaryRestricitons : DietaryRestriction

}