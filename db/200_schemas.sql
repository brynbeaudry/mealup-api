
drop schema if exists app_public cascade;
drop schema if exists app_private cascade;

create schema app_public;
create schema app_private;

grant usage on schema app_public to mealup_visitor;
grant usage on schema app_public to mealup_user;
grant usage on schema app_public to mealup_superuser;
-- DELETE IN PRODUCTION
grant usage on schema app_private to mealup_superuser;

-- This allows inserts without granting permission to the serial primary key column.
alter default privileges in schema app_public grant usage, select on sequences to mealup_visitor;
alter default privileges in schema app_public grant usage, select on sequences to mealup_user;
alter default privileges in schema app_public grant usage, select on sequences to mealup_superuser;
