

-- USER_PROFILES
--------------------------------------------------------------------
alter table app_public.user_profiles enable row level security;
create trigger _100_timestamps
    after insert or update on app_public.user_profiles 
    for each row
    execute procedure app_private.tg__update_timestamps();

create policy select_all on app_public.user_profiles for select using (true);
create policy insert_admin on app_public.user_profiles for insert with check (user_id = app_public.current_user_id());
create policy update_admin on app_public.user_profiles for update using (user_id = app_public.current_user_id() or app_public.current_user_is_superuser());
create policy delete_admin on app_public.user_profiles for delete using (user_id = app_public.current_user_id() or app_public.current_user_is_superuser());
grant select on app_public.user_profiles to mealup_visitor;
grant select on app_public.user_profiles to mealup_user;
grant insert on app_public.user_profiles to mealup_user;
grant update on app_public.user_profiles to mealup_user;
grant delete on app_public.user_profiles to mealup_user;
grant select on app_public.user_profiles to mealup_superuser;
grant insert on app_public.user_profiles to mealup_superuser;
grant update on app_public.user_profiles to mealup_superuser;
grant delete on app_public.user_profiles to mealup_superuser;
        

-- COMPANY_STAFF_REQUESTS
--------------------------------------------------------------------
alter table app_public.company_staff_requests enable row level security;
create trigger _100_timestamps
    after insert or update on app_public.company_staff_requests 
    for each row
    execute procedure app_private.tg__update_timestamps();

create policy select_all on app_public.company_staff_requests for select using (true);
create policy insert_admin on app_public.company_staff_requests for insert with check (user_id = app_public.current_user_id());
create policy update_admin on app_public.company_staff_requests for update using (user_id = app_public.current_user_id() or app_public.current_user_is_superuser());
create policy delete_admin on app_public.company_staff_requests for delete using (user_id = app_public.current_user_id() or app_public.current_user_is_superuser());
grant select on app_public.company_staff_requests to mealup_visitor;
grant select on app_public.company_staff_requests to mealup_user;
grant insert on app_public.company_staff_requests to mealup_user;
grant update on app_public.company_staff_requests to mealup_user;
grant delete on app_public.company_staff_requests to mealup_user;
grant select on app_public.company_staff_requests to mealup_superuser;
grant insert on app_public.company_staff_requests to mealup_superuser;
grant update on app_public.company_staff_requests to mealup_superuser;
grant delete on app_public.company_staff_requests to mealup_superuser;
        
            

-- ITEM_ATTRIBUTES
--------------------------------------------------------------------
alter table app_public.item_attributes enable row level security;

create policy select_all on app_public.item_attributes for select using (true);
create policy insert_admin on app_public.item_attributes for insert with check (app_public.current_user_is_superuser());
create policy update_admin on app_public.item_attributes for update using (app_public.current_user_is_superuser());
create policy delete_admin on app_public.item_attributes for delete using (app_public.current_user_is_superuser());
grant select on app_public.item_attributes to mealup_visitor;
grant select on app_public.item_attributes to mealup_user;
grant select on app_public.item_attributes to mealup_superuser;
grant insert on app_public.item_attributes to mealup_superuser;
grant update on app_public.item_attributes to mealup_superuser;
grant delete on app_public.item_attributes to mealup_superuser;
        

-- ITEM_CATEGORIES
--------------------------------------------------------------------
alter table app_public.item_categories enable row level security;

create policy select_all on app_public.item_categories for select using (true);
create policy insert_admin on app_public.item_categories for insert with check (app_public.current_user_is_superuser());
create policy update_admin on app_public.item_categories for update using (app_public.current_user_is_superuser());
create policy delete_admin on app_public.item_categories for delete using (app_public.current_user_is_superuser());
grant select on app_public.item_categories to mealup_visitor;
grant select on app_public.item_categories to mealup_user;
grant select on app_public.item_categories to mealup_superuser;
grant insert on app_public.item_categories to mealup_superuser;
grant update on app_public.item_categories to mealup_superuser;
grant delete on app_public.item_categories to mealup_superuser;
        
            
        
-- DIETARY_RESTRICTIONS
--------------------------------------------------------------------

grant select on app_public.dietary_restrictions to mealup_visitor;
grant select on app_public.dietary_restrictions to mealup_user;
grant insert on app_public.dietary_restrictions to mealup_user;
grant update on app_public.dietary_restrictions to mealup_user;
grant delete on app_public.dietary_restrictions to mealup_user;
grant select on app_public.dietary_restrictions to mealup_superuser;
grant insert on app_public.dietary_restrictions to mealup_superuser;
grant update on app_public.dietary_restrictions to mealup_superuser;
grant delete on app_public.dietary_restrictions to mealup_superuser;
        
        
-- EVENTS
--------------------------------------------------------------------

grant select on app_public.events to mealup_visitor;
grant select on app_public.events to mealup_user;
grant insert on app_public.events to mealup_user;
grant update on app_public.events to mealup_user;
grant delete on app_public.events to mealup_user;
grant select on app_public.events to mealup_superuser;
grant insert on app_public.events to mealup_superuser;
grant update on app_public.events to mealup_superuser;
grant delete on app_public.events to mealup_superuser;
        
        
-- IMAGES
--------------------------------------------------------------------

grant select on app_public.images to mealup_visitor;
grant select on app_public.images to mealup_user;
grant insert on app_public.images to mealup_user;
grant update on app_public.images to mealup_user;
grant delete on app_public.images to mealup_user;
grant select on app_public.images to mealup_superuser;
grant insert on app_public.images to mealup_superuser;
grant update on app_public.images to mealup_superuser;
grant delete on app_public.images to mealup_superuser;
        
        
-- ITEMS
--------------------------------------------------------------------

grant select on app_public.items to mealup_visitor;
grant select on app_public.items to mealup_user;
grant insert on app_public.items to mealup_user;
grant update on app_public.items to mealup_user;
grant delete on app_public.items to mealup_user;
grant select on app_public.items to mealup_superuser;
grant insert on app_public.items to mealup_superuser;
grant update on app_public.items to mealup_superuser;
grant delete on app_public.items to mealup_superuser;
        
        
-- ITEM_CATEGORY_ATTRIBUTES
--------------------------------------------------------------------

grant select on app_public.item_category_attributes to mealup_visitor;
grant select on app_public.item_category_attributes to mealup_user;
grant insert on app_public.item_category_attributes to mealup_user;
grant update on app_public.item_category_attributes to mealup_user;
grant delete on app_public.item_category_attributes to mealup_user;
grant select on app_public.item_category_attributes to mealup_superuser;
grant insert on app_public.item_category_attributes to mealup_superuser;
grant update on app_public.item_category_attributes to mealup_superuser;
grant delete on app_public.item_category_attributes to mealup_superuser;
        
        
-- ITEM_PORTIONS
--------------------------------------------------------------------

grant select on app_public.item_portions to mealup_visitor;
grant select on app_public.item_portions to mealup_user;
grant insert on app_public.item_portions to mealup_user;
grant update on app_public.item_portions to mealup_user;
grant delete on app_public.item_portions to mealup_user;
grant select on app_public.item_portions to mealup_superuser;
grant insert on app_public.item_portions to mealup_superuser;
grant update on app_public.item_portions to mealup_superuser;
grant delete on app_public.item_portions to mealup_superuser;
        
        
-- LOCATIONS
--------------------------------------------------------------------

grant select on app_public.locations to mealup_visitor;
grant select on app_public.locations to mealup_user;
grant insert on app_public.locations to mealup_user;
grant update on app_public.locations to mealup_user;
grant delete on app_public.locations to mealup_user;
grant select on app_public.locations to mealup_superuser;
grant insert on app_public.locations to mealup_superuser;
grant update on app_public.locations to mealup_superuser;
grant delete on app_public.locations to mealup_superuser;
        
        
-- ORDERS
--------------------------------------------------------------------

grant select on app_public.orders to mealup_visitor;
grant select on app_public.orders to mealup_user;
grant insert on app_public.orders to mealup_user;
grant update on app_public.orders to mealup_user;
grant delete on app_public.orders to mealup_user;
grant select on app_public.orders to mealup_superuser;
grant insert on app_public.orders to mealup_superuser;
grant update on app_public.orders to mealup_superuser;
grant delete on app_public.orders to mealup_superuser;
        
        
-- ORDER_ITEMS
--------------------------------------------------------------------

grant select on app_public.order_items to mealup_visitor;
grant select on app_public.order_items to mealup_user;
grant insert on app_public.order_items to mealup_user;
grant update on app_public.order_items to mealup_user;
grant delete on app_public.order_items to mealup_user;
grant select on app_public.order_items to mealup_superuser;
grant insert on app_public.order_items to mealup_superuser;
grant update on app_public.order_items to mealup_superuser;
grant delete on app_public.order_items to mealup_superuser;
        
        
-- PAYMENTS
--------------------------------------------------------------------

grant select on app_public.payments to mealup_visitor;
grant select on app_public.payments to mealup_user;
grant insert on app_public.payments to mealup_user;
grant update on app_public.payments to mealup_user;
grant delete on app_public.payments to mealup_user;
grant select on app_public.payments to mealup_superuser;
grant insert on app_public.payments to mealup_superuser;
grant update on app_public.payments to mealup_superuser;
grant delete on app_public.payments to mealup_superuser;
        
        
-- SOCIAL_HANDLE
--------------------------------------------------------------------

grant select on app_public.social_handle to mealup_visitor;
grant select on app_public.social_handle to mealup_user;
grant insert on app_public.social_handle to mealup_user;
grant update on app_public.social_handle to mealup_user;
grant delete on app_public.social_handle to mealup_user;
grant select on app_public.social_handle to mealup_superuser;
grant insert on app_public.social_handle to mealup_superuser;
grant update on app_public.social_handle to mealup_superuser;
grant delete on app_public.social_handle to mealup_superuser;
        
        
-- USERS
--------------------------------------------------------------------

grant select on app_public.users to mealup_visitor;
grant select on app_public.users to mealup_user;
grant insert on app_public.users to mealup_user;
grant update on app_public.users to mealup_user;
grant delete on app_public.users to mealup_user;
grant select on app_public.users to mealup_superuser;
grant insert on app_public.users to mealup_superuser;
grant update on app_public.users to mealup_superuser;
grant delete on app_public.users to mealup_superuser;
        
    