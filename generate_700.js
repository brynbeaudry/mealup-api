const fs = require('fs')
const readline = require('readline');
const entities = fs
        .readdirSync(__dirname + '/src/entities')
        .filter(fn => fn !== "index.ts")
        .filter(fn => fn !== "entity_template.ts")
        .filter(fn => fn.match(/^[^.].*\.ts$/));


// Gets all major table names
async function getAllTableNames() {
    let allTables = [];
    for await (const file of entities) {
        //var contents = fs.readFileSync(__dirname + '/src/entities/' + file, 'utf8');
        const fileStream = fs.createReadStream(__dirname + '/src/entities/' + file);
        //console.log('filestream', fileStream)

        const rl = readline.createInterface({
            input: fileStream,
            crlfDelay: Infinity
        });
        // Note: we use the crlfDelay option to recognize all instances of CR LF
        // ('\r\n') in input.txt as a single line break.

        for await (const line of rl) {
            // Each line in input.txt will be successively available here as `line`.
            //console.log(`Line from file: ${line}`);
            const entRegex = new RegExp(/@Entity\('(\w+)',/, "gm");
            const entMatch = entRegex.exec(line);
            const joinRegex = new RegExp(/@JoinTable\(\{name \: '(\w+)'/, "gm"); //@JoinTable({name : 'item_category_attributes'})
            const joinMatch = joinRegex.exec(line);
            
            if(entMatch){
                //console.log(`Line from file: ${line}`);
                //console.log(`match from line: ${match[1]}`);
                allTables.push(entMatch[1]);
            }
            if(joinMatch){
                //console.log(`Line from file: ${line}`);
                //console.log(`match from line: ${match[1]}`);
                allTables.push(joinMatch[1]);
            }
        }
    }
    return allTables
}


const userOwnedTables = [
    // TODO: There are many more
    'user_profiles',
    'company_staff_requests'
]

// Must be admin of company
const companyOwned = [
    'company_staff',
    'companies'
]

// 'events' is its own thing
// 'images' are uploaded by users. We may just be keeping track of who uploaded it.
// you'd need compoany permissions to edit as comapny's photo, event permissions to edit the event photo, item permissions to change an item photo, permissions on the userprofile to edit the userprofile photo


const adminOnlyTables = [
    'item_attributes',
    'item_categories'
]

// There sohuld be a created_by, user_owed, company owned
//Company is the hardest part
/*
User can make Event for company if the user's id, referenced against the is_staff attr/is_admin attr matches with the id of that event's associated company
Only Company Admins can update the Company
User can make Item for company Event if is_staff/is_admin attr matches event company id
Users can create the Orders and delete them with their token. Editing them would would be risky. Admins shouldn't be able to edit them either.
Users can create payments and read only


Anyone can make a CompanyStaffRequest entry, 
but only users with is_admin matching for the company id, can crud the CompanyStaff with their token, so they'll see the request, 
and then they'll have an area to write the request into a compmany staff entry. This will keep track of the real company staff,
and a every write to that table updates the user roles table.

*/


function writeCompanyOwnedTablePermissions() {
    let string = ''

    for (const tablename of companyOwned) {
        string += `

-- ${tablename.toUpperCase()}
--------------------------------------------------------------------
alter table app_public.${tablename} enable row level security;
create trigger _100_timestamps
    after insert or update on app_public.${tablename} 
    for each row
    execute procedure app_private.tg__update_timestamps();

create policy select_all on app_public.${tablename} for select using (true);
create policy insert_admin on app_public.${tablename} for insert with check (true);
create policy update_admin on app_public.${tablename} for update using (company_id = app_public.app_public.current_user_is_staff() or company_id = app_public.app_public.current_user_is_admin() or app_public.current_user_is_superuser());
create policy delete_admin on app_public.${tablename} for delete using (company_id = app_public.app_public.current_user_is_staff() or company_id = app_public.app_public.current_user_is_admin() or app_public.current_user_is_superuser());
grant select on app_public.${tablename} to mealup_visitor;
grant select on app_public.${tablename} to mealup_user;
grant insert on app_public.${tablename} to mealup_user;
grant update on app_public.${tablename} to mealup_user;
grant delete on app_public.${tablename} to mealup_user;
grant select on app_public.${tablename} to mealup_superuser;
grant insert on app_public.${tablename} to mealup_superuser;
grant update on app_public.${tablename} to mealup_superuser;
grant delete on app_public.${tablename} to mealup_superuser;
        `;
    }
    return string
}

function writeUserOwnedTablePermissions() {
    let string = ''

    for (const tablename of userOwnedTables) {
        string += `

-- ${tablename.toUpperCase()}
--------------------------------------------------------------------
alter table app_public.${tablename} enable row level security;
create trigger _100_timestamps
    after insert or update on app_public.${tablename} 
    for each row
    execute procedure app_private.tg__update_timestamps();

create policy select_all on app_public.${tablename} for select using (true);
create policy insert_admin on app_public.${tablename} for insert with check (user_id = app_public.current_user_id());
create policy update_admin on app_public.${tablename} for update using (user_id = app_public.current_user_id() or app_public.current_user_is_superuser());
create policy delete_admin on app_public.${tablename} for delete using (user_id = app_public.current_user_id() or app_public.current_user_is_superuser());
grant select on app_public.${tablename} to mealup_visitor;
grant select on app_public.${tablename} to mealup_user;
grant insert on app_public.${tablename} to mealup_user;
grant update on app_public.${tablename} to mealup_user;
grant delete on app_public.${tablename} to mealup_user;
grant select on app_public.${tablename} to mealup_superuser;
grant insert on app_public.${tablename} to mealup_superuser;
grant update on app_public.${tablename} to mealup_superuser;
grant delete on app_public.${tablename} to mealup_superuser;
        `;
    }
    return string
}

function writeAdminOnlyTablePermissions() {
    var string = ''
    for (const tablename of adminOnlyTables) {
        string += `

-- ${tablename.toUpperCase()}
--------------------------------------------------------------------
alter table app_public.${tablename} enable row level security;

create policy select_all on app_public.${tablename} for select using (true);
create policy insert_admin on app_public.${tablename} for insert with check (app_public.current_user_is_superuser());
create policy update_admin on app_public.${tablename} for update using (app_public.current_user_is_superuser());
create policy delete_admin on app_public.${tablename} for delete using (app_public.current_user_is_superuser());
grant select on app_public.${tablename} to mealup_visitor;
grant select on app_public.${tablename} to mealup_user;
grant select on app_public.${tablename} to mealup_superuser;
grant insert on app_public.${tablename} to mealup_superuser;
grant update on app_public.${tablename} to mealup_superuser;
grant delete on app_public.${tablename} to mealup_superuser;
        `;
    }
    return string
}

function writeOtherTablePermissions(otherTables) {
    let string = ''
    for (const tablename of otherTables) {
        string += `
        
-- ${tablename.toUpperCase()}
--------------------------------------------------------------------

grant select on app_public.${tablename} to mealup_visitor;
grant select on app_public.${tablename} to mealup_user;
grant insert on app_public.${tablename} to mealup_user;
grant update on app_public.${tablename} to mealup_user;
grant delete on app_public.${tablename} to mealup_user;
grant select on app_public.${tablename} to mealup_superuser;
grant insert on app_public.${tablename} to mealup_superuser;
grant update on app_public.${tablename} to mealup_superuser;
grant delete on app_public.${tablename} to mealup_superuser;
        `;
    }
    return string
}


function writePermsions(otherTables){

    return `${writeUserOwnedTablePermissions()}
            ${writeAdminOnlyTablePermissions()}
            ${writeOtherTablePermissions(otherTables)}
    `;
}
async function writeFile() {
    let allTables = await getAllTableNames()
    //console.log('All Table Names:', allTables)
    const otherTables = allTables
        .filter(word => !userOwnedTables.includes(word))
        .filter(word => !adminOnlyTables.includes(word))
        .filter(word => !companyOwned.includes(word))
    fs.writeFile("./db/700_mealup.sql", writePermsions(otherTables), function(err) {
        if(err){
            return console.log(err)
        }
        console.log("The file was created")
    });
};
writeFile();
