#!/bin/bash
set -e
# change this when creating the .env for production

if [ -x .env ]; then
  . ./.env
  if [ "$SUPERUSER_PASSWORD" = "" ]; then
    echo ".env already exists, but it doesn't define SUPERUSER_PASSWORD - aborting!"
    exit 1;
  fi
  if [ "$AUTH_USER_PASSWORD" = "" ]; then
    echo ".env already exists, but it doesn't define AUTH_USER_PASSWORD - aborting!"
    exit 1;
  fi
  echo "Configuration already exists, using existing secrets."
else
  # This will generate passwords that are safe to use in envvars without needing to be escaped:
  SUPERUSER_PASSWORD="$(openssl rand -base64 30 | tr '+/' '-_')"
  AUTH_USER_PASSWORD="$(openssl rand -base64 30 | tr '+/' '-_')"

  # Change this is the production env
  NODE_ENV="development"
  PRODUCTION_ROOT_PASSWORD="PrimeBananas"
  PRODUCTION_CONNECTION_STRING="aws-mealup-rds.cteuiovvf6bm.us-east-2.rds.amazonaws.com"
  LOCAL_ROOT_PASSWORD="$SUPERUSER_PASSWORD"
  LOCAL_CONNECTION_STRING="localhost"
  AWS_CONNECTION_URL="postgres://root:\$PRODUCTION_ROOT_PASSWORD@\$PRODUCTION_CONNECTION_STRING:5432/mealup"
  LOCAL_CONNECTION_URL="postgres://root:\$LOCAL_ROOT_PASSWORD@\$LOCAL_CONNECTION_STRING:5432/mealup"

  if [ "$NODE_ENV" = "development" ]; then
    CONNECTION_STRING="$LOCAL_CONNECTION_STRING"
    ROOT_PASSWORD="$LOCAL_ROOT_PASSWORD"
    CONNECTION_URL="$LOCAL_CONNECTION_URL"
  else
    CONNECTION_STRING="$PRODUCTION_CONNECTION_STRING"
    ROOT_PASSWORD="$PRODUCTION_ROOT_PASSWORD"
    CONNECTION_URL="$AWS_CONNECTION_URL"
  fi

  # This is our '.env' config file, we're writing it now so that if something goes wrong we won't lose the passwords.
  cat >> .env <<CONFIG
# This is a development environment (production wouldn't write envvars to a file)
# Change this is the production env
export NODE_ENV="$NODE_ENV"

# Set variables base on node env local: prod
export CONNECTION_STRING="$CONNECTION_STRING"
export ROOT_PASSWORD="$ROOT_PASSWORD"
export CONNECTION_URL="$CONNECTION_URL"

# Password for the 'mealup' user, which owns the database
export SUPERUSER_PASSWORD="$SUPERUSER_PASSWORD"

# Password for the 'mealup_authenticator' user, which has very limited
# privileges, but can switch into mealup_visitor
export AUTH_USER_PASSWORD="$AUTH_USER_PASSWORD"

# This secret is used for signing cookies
export SECRET="$(openssl rand -base64 48)"

# This secret is used for signing JWT tokens (we don't use this by default)
export JWT_SECRET="$(openssl rand -base64 48)"


# These are the connection strings for the DB and the test DB.
export ROOT_DATABASE_URL="postgresql://mealup:\$SUPERUSER_PASSWORD@\$CONNECTION_STRING:5432/mealup"
export AUTH_DATABASE_URL="postgresql://mealup_authenticator:\$AUTH_USER_PASSWORD@\$CONNECTION_STRING:5432/mealup"
export TEST_ROOT_DATABASE_URL="postgresql://mealup:\$SUPERUSER_PASSWORD@\$CONNECTION_STRING:5432/mealup_test"
export TEST_AUTH_DATABASE_URL="postgresql://mealup_authenticator:\$AUTH_USER_PASSWORD@\$CONNECTION_STRING:5432/mealup_test"

# This port is the one you'll connect to
export PORT=8349

# This is the port that create-react-app runs as, don't connect to it directly
# export CLIENT_PORT=8350

# This is needed any time we use absolute URLs, e.g. for OAuth callback URLs
export ROOT_DOMAIN="localhost:\$PORT"
export ROOT_URL="http://\$ROOT_DOMAIN"

# Our session store uses redis
# export REDIS_URL="redis://localhost/3"

#   Facebook Login
#   Name: mealup
#   Homepage URL: http://localhost:8349
#   Authorization callback URL: http://localhost:8349/auth/facebook/callback
#
# Client ID:
export FACEBOOK_KEY=""
# Client Secret:
export FACEBOOK_SECRET=""

#   Google OAuth2 Login
#   Name: mealup
#   Homepage URL: http://localhost:8349
#   Authorization callback URL: http://localhost:8349/auth/google/callback
#
# Client ID:
export GOOGLE_KEY=""
# Client Secret:
export GOOGLE_SECRET=""

CONFIG
  echo "Passwords generated and configuration written to .env"

  # To source our .env file from the shell it has to be executable.
  chmod +x .env

  . ./.env
  source ./.env
fi


echo "Installing or reinstalling the roles and database..."
# Now we can reset the database
if [ $NODE_ENV = "development" ]; then
    reset_db="psql -X -v ON_ERROR_STOP=1 template1"
    aws_rds_superuser_to_db_grant="--"
    aws_db_to_root_grant="--"
    aws_rds_superuser_to_auth_grant="--"
else
    # Production
    reset_db="psql --host aws-mealup-rds.cteuiovvf6bm.us-east-2.rds.amazonaws.com --port 5432 --username root --dbname postgres -X -v ON_ERROR_STOP=1 template1"
    # following allows aws to create tables
    aws_rds_superuser_to_db_grant="grant rds_superuser to mealup;"
    aws_db_to_root_grant="grant mealup to root;"
    aws_rds_superuser_to_auth_grant="grant rds_superuser to mealup_authenticator;"
fi
$reset_db <<SQL
-- RESET database
--echo-all
DROP DATABASE IF EXISTS mealup;
DROP DATABASE IF EXISTS mealup_test;
DROP DATABASE IF EXISTS mealup_org_demo;
DROP ROLE IF EXISTS mealup_visitor;
DROP ROLE IF EXISTS mealup_user;
DROP ROLE IF EXISTS mealup_superuser;
DROP ROLE IF EXISTS mealup_authenticator;
DROP ROLE IF EXISTS mealup;

-- Now to set up the database cleanly:

-- Ref: https://devcenter.heroku.com/articles/heroku-postgresql#connection-permissions

-- This is the root role for the database
-- You may not be able to grant super user in production. You can look at agora for clues
CREATE ROLE mealup WITH LOGIN PASSWORD '${SUPERUSER_PASSWORD}' SUPERUSER;  
-- only do this in aws production 
${aws_rds_superuser_to_db_grant}
${aws_db_to_root_grant}

-- This is the no-access role that PostGraphile will run as by default
CREATE ROLE mealup_authenticator WITH LOGIN PASSWORD '${AUTH_USER_PASSWORD}' NOINHERIT;

-- This is the role that PostGraphile will switch to (from mealup_authenticator) during a transaction
CREATE ROLE mealup_visitor;

-- This is the role for registered users
CREATE ROLE mealup_user;

-- This is the role for registered users who are admins
CREATE ROLE mealup_superuser;

-- This enables PostGraphile to switch from mealup_authenticator to mealup_visitor, mealup_user, and mealup admin
-- only do this in aws production 
${aws_rds_superuser_to_auth_grant}
GRANT mealup_visitor TO mealup_authenticator;
GRANT mealup_user TO mealup_authenticator;
GRANT mealup_superuser TO mealup_authenticator;

\echo Granted stuff to authenticator


-- Here's our main database
CREATE DATABASE mealup OWNER mealup;
\echo Created mealup db
REVOKE ALL ON DATABASE mealup FROM PUBLIC;
GRANT CONNECT ON DATABASE mealup TO mealup;
GRANT CONNECT ON DATABASE mealup TO mealup_authenticator;
GRANT ALL ON DATABASE mealup TO mealup;

-- Some extensions require superuser privileges, so we create them before migration time.
\\connect mealup
CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
CREATE EXTENSION IF NOT EXISTS citext;
CREATE EXTENSION IF NOT EXISTS pgcrypto;

-- This is a copy of the setup above for our test database
CREATE DATABASE mealup_test OWNER mealup;
REVOKE ALL ON DATABASE mealup_test FROM PUBLIC;
GRANT CONNECT ON DATABASE mealup_test TO mealup;
GRANT CONNECT ON DATABASE mealup_test TO mealup_authenticator;
GRANT ALL ON DATABASE mealup_test TO mealup;
\\connect mealup_test
CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
CREATE EXTENSION IF NOT EXISTS citext;
CREATE EXTENSION IF NOT EXISTS pgcrypto;
SQL

[[ $NODE_ENV = "development" ]] && run_db_scripts="--dbname mealup" || run_db_scripts="--host aws-mealup-rds.cteuiovvf6bm.us-east-2.rds.amazonaws.com --port 5432 --username root --dbname mealup"

# TODO: Audit tables created
echo "Roles and databases created, now sourcing the initial database schema"
psql -X1 -v ON_ERROR_STOP=1 $run_db_scripts -f db/100_jobs.sql
psql -X1 -v ON_ERROR_STOP=1 $run_db_scripts -f db/200_schemas.sql
psql -X1 -v ON_ERROR_STOP=1 $run_db_scripts -f db/300_utils.sql

# Here, we run a special yarn script that uses type orm and a ts node interpreter
# This updates Our entities from our TypeOrm definitions
yarn init_entities
node generate_700.js

psql -X1 -v ON_ERROR_STOP=1 $run_db_scripts -f db/400_users.sql
psql -X1 -v ON_ERROR_STOP=1 $run_db_scripts -f db/700_mealup.sql


echo "Dumping full SQL schema to data/schema.sql"
#./scripts/schema_dump

echo "Exporting GraphQL schema to data/schema.graphql and data/schema.json"
#yarn postgraphile -X --export-schema-graphql data/schema.graphql --export-schema-json data/schema.json

# All done
echo "✅ Setup success"
